# Scheduler for macOS - Update Homebrew

### Description

The script __update_homebrew.sh__ will update & upgrade homebrew.

The scheduled task (local.updatehomebrew.plist) is scheduled to run at 9:00 AM every day.
If you want to change the timing, you can update the file, under *StartCalendarInterval* section.

```xml
        <key>StartCalendarInterval</key>
        <dict>
            <key>Hour</key>
            <integer>9</integer>
            <key>Minute</key>
            <integer>0</integer>
        </dict>
```

### Installation

The script __install_script.sh__ will copy the following files to their respective locations.

* *update_homebrew.sh* to "$HOME/Scripts"
* *local.updatehomebrew.plist* to "$HOME/Library/LaunchAgents"

You can change the locations in *install_script.sh*.

To install, run:

```bash
./install_script.sh
```

### Log file

The script writes a log file at this location:

*$HOME/Library/Logs/brew_update.log*

