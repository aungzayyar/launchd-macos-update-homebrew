#!/usr/bin/env bash

SCRIPTS_DIR="${HOME}/Scripts"
SCRIPT_FILE="update_homebrew.sh"
AGENT_DIR="${HOME}/Library/LaunchAgents"
TASK_FILE="local.updatehomebrew.plist"

# Copy the script to ${SCRIPTS_DIR} directory
if [[ ! -d "${SCRIPTS_DIR}" ]]
then
    mkdir "${SCRIPTS_DIR}"
fi
cp ${SCRIPT_FILE} ${SCRIPTS_DIR}/

# Copy the plist file to ${AGENT_DIR} directory
if [[ ! -d "${AGENT_DIR}" ]]
then
    mkdir "${AGENT_DIR}"
fi

sed -e "s%/Me/%/${USER}/%" ${TASK_FILE} > ${AGENT_DIR}/${TASK_FILE}

# Make sure existing task is unloaded
launchctl unload "${AGENT_DIR}/${TASK_FILE}"

# Load the script
launchctl load "${AGENT_DIR}/${TASK_FILE}"